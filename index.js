class Command {
    constructor(options, executor){
        this.data = {options, executor: (Promise.resolve(executor) == executor ? executor : (async(...args)=>{return executor(...args)}))};
    }
    setOption(option, data){
        this.data.options[option] = data;
    }
    async execute(context){
        return await (this.executor)(context);
    }
}

class DJSCF {
    constructor(bot){
        this.bot = bot;
        this.commands = new (require("discord.js").Collection)();
    }
    setPrefix(prefix){
        this.prefix = prefix;
        if(typeof prefix === "string"){
            this.prefixResolver = (async()=>{return message.content.startsWith(prefix);});
        }else{
            if(Promise.resolve(prefix) == prefix){
                this.prefixResolver = prefix;
            }else{
                if(prefix.type == "mention"){
                    this.prefixResolver = (async(bot, message)=>{
                        let rgx = new RegExp(`^<@!?${bot.user.id}>.*$`, "gi");
                        if(!rgx.test(message.content))return ".".repeat(3000);
                        return (message.content.startsWith("<@!")?`<@!${bot.user.id}>`:`<@${bot.user.id}>`);
                    });
                }else{
                    this.prefixResolver = (async(bot, message)=>{return prefix(bot, message);});
                }
            }
        }
    }
    addCommand(command){
        let data = command.data;
        data.execute = data.executor;
        this.commands.set(command.data.options.name, data);
    }
    removeCommand(commandName){
        this.commands.delete(this.commands.get(commandName)?commandName:this.commands.find(cmd=>cmd.aliases && cmd.aliases.includes(commandName).name));
    }
    setup(options){
        if(!options)options = {};
        if(!this.prefixResolver){
            throw new Error("Prefix is not set.");
        }
        this.bot.on("message", async(message)=>{
            if(message.author.bot || (options.nodms == true && message.channel.type == "dm") || !message.content)return;
            let prefix = await this.prefixResolver(this.bot, message);
            if(!message.content.startsWith(prefix))return;
            let args = message.content.slice(prefix.length).split(/ +/g);
            if(args[0] == "")args.shift();
            let commandName = args.shift();
            let command = this.commands.get(commandName) || this.commands.find(cmd => cmd.options.aliases && cmd.options.aliases.includes(commandName));
            if(!command)return;
            if(command.options.disabled)return;
            if(command.options.ownerOnly && !(!options.owners?[]:(Array.isArray(options.owners)?options.owners:[options.owners])).includes(message.author.id))return;
            await command.execute({bot: message.client, message, args});
        });
    }
}

module.exports = {Command, DJSCF};