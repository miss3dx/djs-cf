## DJS-CF - Discord.JS (very tiny) Command Framework.
### Example usage:
```javascript
// creating instance of DJSCF.
let djscf = new (require("djs-cf").DJSCF)(<discord.js Client instance>);
// setting prefix.
djscf.setPrefix("!");
djscf.setPrefix({type: "mention"});
djscf.setPrefix((bot, message)=>{
    //sync stuff.
    return prefix;
});
djscf.setPrefix(async(bot, message)=>{
    // async stuff.
    return await MyDatabase.getGuild(message.guild.id).getPrefix();
});
// adding commands.
djscf.addCommand(new(require("djs-cf").Command)({
    name: "ping",
    aliases: ["pong"],
    description: "Ping-pong.",
    otherkey: "othervalue",
    ownerOnly: false
}, async(context)=>{
    await context.message.channel.send(":ping_pong: Pong!");
}));
// launching.
djscf.setup({owners: ["id1", "id2"]});
```
This was mostly done just for me but you can use it in your projects. :)